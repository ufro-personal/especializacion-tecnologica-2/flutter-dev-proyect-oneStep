import 'package:flutter/material.dart';
import '/app/pages/verification/verification_view.dart';

class PayView extends StatelessWidget {
  final String? precio;
  final String? nombreComida;

  const PayView({Key? key, this.precio, this.nombreComida}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(children: [
            Container(
                height: 100,
                width: 220,
                child: Image(image: AssetImage("assets/logo.png"))),
            Container(
                height: 150,
                padding: EdgeInsets.only(bottom: 10),
                child: Image(image: AssetImage("assets/credit_card.png"))),
            Container(
              width: double.infinity,
              height: size.height * 0.641,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: Column(
                children: [
                  SizedBox(
                    height: 45,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.height * 0.1),
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => VerificationView(
                                          nombreComida: nombreComida!,
                                          precio: precio!,
                                        ))));
                          },
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Ir a pagar"),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.orange),
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.height * 0.1),
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () {},
                          child: Text("Ver datos"),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.orange),
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.height * 0.1),
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () {},
                          child: Text("Eliminar tarjeta"),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.orange),
                          )),
                    ),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}
