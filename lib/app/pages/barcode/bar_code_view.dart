import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:slide_countdown/slide_countdown.dart';

class BarCodeView extends StatelessWidget {
  final String precio;
  final String nombreComida;

  const BarCodeView(
      {Key? key, required this.precio, required this.nombreComida})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    var encoded = utf8.encode(this.precio + this.nombreComida);
    var decoded = utf8.decode(encoded);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            children: [
              Container(
                  height: 100,
                  width: 220,
                  child: Image(image: AssetImage("assets/logo.png"))),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 300,
                width: 200,
                child: Column(
                  children: [
                    Text(this.nombreComida,
                        style: TextStyle(
                            fontSize: 13, fontWeight: FontWeight.bold)),
                    Text(this.precio,
                        style: TextStyle(
                            fontSize: 13, fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 200,
                      width: 200,
                      color: Colors.white,
                      child: QrImage(data: decoded),
                    ),
                    SlideCountdown(
                      duration: const Duration(minutes: 60),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
