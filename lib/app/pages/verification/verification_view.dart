import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../barcode/bar_code_view.dart';

class VerificationView extends StatefulWidget {
  final String precio;
  final String nombreComida;

  const VerificationView(
      {Key? key, required this.precio, required this.nombreComida})
      : super(key: key);
  @override
  State<VerificationView> createState() =>
      _VerificationViewState(nombreComida: nombreComida, precio: precio);
}

class _VerificationViewState extends State<VerificationView> {
  final codeController1 = TextEditingController();
  final codeController2 = TextEditingController();
  final codeController3 = TextEditingController();
  final codeController4 = TextEditingController();

  final String precio;
  final String nombreComida;

  _VerificationViewState({required this.precio, required this.nombreComida});

  @override
  void dispose() {
    codeController1.dispose();
    codeController2.dispose();
    codeController3.dispose();
    codeController4.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  height: 100,
                  width: 220,
                  child: Image(image: AssetImage("assets/logo.png"))),
              Text(
                "Inserte PIN",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    color: Colors.white.withOpacity(0.4),
                    width: 50,
                    child: TextField(
                      obscureText: true,
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      controller: codeController1,
                      keyboardType: TextInputType.phone,
                      textAlign: TextAlign.center,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white.withOpacity(0.4),
                    width: 50,
                    child: TextField(
                      obscureText: true,
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      controller: codeController2,
                      keyboardType: TextInputType.phone,
                      textAlign: TextAlign.center,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white.withOpacity(0.4),
                    width: 50,
                    child: TextField(
                      obscureText: true,
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      controller: codeController3,
                      keyboardType: TextInputType.phone,
                      textAlign: TextAlign.center,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white.withOpacity(0.4),
                    width: 50,
                    child: TextField(
                      obscureText: true,
                      controller: codeController4,
                      keyboardType: TextInputType.phone,
                      textAlign: TextAlign.center,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 300,
              ),
              Container(
                width: 300,
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => BarCodeView(
                                    nombreComida: nombreComida,
                                    precio: precio,
                                  ))));
                    },
                    child: Text(
                      "Aceptar",
                      style: TextStyle(
                          color: Colors.orange, fontWeight: FontWeight.bold),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.white),
                    )),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
