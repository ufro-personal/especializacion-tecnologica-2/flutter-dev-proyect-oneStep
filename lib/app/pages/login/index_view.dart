import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:plantilla/app/pages/home/home_view.dart';
import 'package:plantilla/domain/repositories/login_repository.dart';
import 'package:qr_flutter/qr_flutter.dart';

class IndexView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final googleLogin = LoginRepository();

    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            children: [
              Container(
                  height: 200,
                  width: 400,
                  child: Image(image: AssetImage("assets/logo.png"))),
              Container(
                  width: double.infinity,
                  height: size.height * 0.705,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(children: [
                    SizedBox(
                      height: 50,
                    ),
                    Text("Bienvenidos!",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w900)),
                    SizedBox(
                      height: 80,
                    ),
                    Container(
                        width: 210,
                        child: SignInButton(
                          Buttons.Google,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          padding: EdgeInsets.all(5),
                          onPressed: () {
                            googleLogin.loginGoogle();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => HomeView())));
                          },
                          text: "Inicia sesión con Google",
                        )),
                  ]))
            ],
          ),
        ),
      ),
    );
  }
}
