import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:plantilla/app/widgets/menu_dia.dart';
import 'package:horizontal_list/horizontal_list.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listaSemana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"];
    final size = MediaQuery.of(context).size;
    User? user = FirebaseAuth.instance.currentUser;

    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.only(top: 25),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: size.width * 0.1),
                  child: Text(
                    "Ufro Menú",
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  height: size.height * 0.167,
                  child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.only(left: 140, right: 50),
                      scrollDirection: Axis.horizontal,
                      itemCount: listaSemana.length,
                      itemBuilder: (context, int i) {
                        return Padding(
                          padding: EdgeInsets.only(right: 100),
                          child: Container(
                            child: Center(
                              child: Container(
                                width: size.width * 0.20,
                                height: size.height * 0.05,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Center(
                                  child: Text(
                                    listaSemana[i],
                                  ),
                                ),
                              ),
                            ),
                          ), //envolver texto en container
                        );
                      }),
                ),
                Container(
                    width: double.infinity,
                    height: size.height * 0.713,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Column(children: [
                      SizedBox(
                        height: 45,
                      ),
                      Text("El menú de hoy es",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 45,
                      ),
                      MenuDia(
                          imagen:
                              "https://colacionesprovi.cl/wp-content/uploads/2021/11/cazuela-vacuno-1024x1024.png",
                          precio: "2.300",
                          nombreComida: "Estándar"),
                      MenuDia(
                          imagen:
                              "https://gurepoke.eus/wp-content/uploads/2021/08/GurePoke-LURRA-arroz-705x705.png",
                          precio: "2.200",
                          nombreComida: "Hipocalórico"),
                      MenuDia(
                          imagen:
                              "https://s3-eu-central-1.amazonaws.com/bk-cl-demo.menu.app/wp-media-folder-burger-king-chile//home/ubuntu/wordpress/web/app/uploads/sites/13/2021/10/Fotos-sitio-BK_400px_Hamburguesa-doble-italiana.png",
                          precio: "2.100",
                          nombreComida: "Hipercalórico"),
                    ]))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
