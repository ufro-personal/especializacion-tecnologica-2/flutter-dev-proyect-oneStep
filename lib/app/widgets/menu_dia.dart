import 'package:flutter/material.dart';
import 'package:plantilla/app/pages/pay/pay_view.dart';

class MenuDia extends StatelessWidget {
  final String nombreComida;
  final String imagen;
  final String precio;

  const MenuDia(
      {Key? key,
      required this.nombreComida,
      required this.imagen,
      required this.precio})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(bottom: size.height * 0.05),
      child: GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PayView(
                      nombreComida: nombreComida,
                      precio: precio,
                    ))),
        child: Container(
          width: size.width * 0.7,
          height: size.height * 0.1,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                    color: Color.fromARGB(255, 197, 197, 197),
                    offset: Offset(3, 2),
                    blurRadius: 6)
              ]),
          child: Padding(
            padding: EdgeInsets.only(
                left: size.width * 0.05, right: size.width * 0.05),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: size.width * 0.17,
                    height: size.height * 0.08,
                    child: Image(
                        fit: BoxFit.cover, image: NetworkImage(this.imagen)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: size.height * 0.024),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(this.nombreComida,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w700)),
                        Text("\$" + precio,
                            style: TextStyle(
                                color: Color.fromARGB(255, 109, 109, 109),
                                fontSize: 15,
                                fontWeight: FontWeight.w500)),
                      ],
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
