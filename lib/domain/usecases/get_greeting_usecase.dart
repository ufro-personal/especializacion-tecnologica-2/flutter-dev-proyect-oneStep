import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:plantilla/domain/entities/example.dart';
import 'package:plantilla/domain/repositories/example_repository.dart';

class GetGreetingUseCase
    extends UseCase<GetGreetingUseCaseResponse, GetGreetingUseCaseParams> {
  ExampleRepository exampleRepository;

  GetGreetingUseCase(this.exampleRepository);

  @override
  Future<Stream<GetGreetingUseCaseResponse?>> buildUseCaseStream(
      GetGreetingUseCaseParams? params) async {
    final controller = StreamController<GetGreetingUseCaseResponse>();
    try {
      final Example example =
          await exampleRepository.getGreeting(params!.greeting);
      logger.finest('GetGreetingUseCase successful');
      controller.add(GetGreetingUseCaseResponse(example));
      controller.close();
    } catch (e) {
      logger.severe('GetGreetingUseCase error');
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetGreetingUseCaseParams {
  final String greeting;
  GetGreetingUseCaseParams(this.greeting);
}

class GetGreetingUseCaseResponse {
  final Example example;
  GetGreetingUseCaseResponse(this.example);
}
