import 'dart:async';

import 'package:plantilla/domain/entities/example.dart';

abstract class ExampleRepository {
  Future<Example> getGreeting(String greeting);
}
