import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'app/pages/home/home_view.dart';
import 'app/pages/pay/pay_view.dart';
import 'app/pages/login/index_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme:
          ThemeData(scaffoldBackgroundColor: Color.fromARGB(255, 207, 113, 25)),
      initialRoute: "index",
      routes: {
        "home": (context) => HomeView(),
        "pay": (context) => PayView(),
        "index": (context) => IndexView()
      },
    );
  }
}
