import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

import 'package:plantilla/domain/entities/example.dart';
import 'package:plantilla/domain/repositories/example_repository.dart';

class DataRemoteExampleRepository implements ExampleRepository {
  static final DataRemoteExampleRepository _instance =
      DataRemoteExampleRepository._internal();
  Logger _logger;

  DataRemoteExampleRepository._internal()
      : _logger = Logger('DataRemoteExampleRepository');

  factory DataRemoteExampleRepository() => _instance;

  @override
  Future<Example> getGreeting(String greeting) async {
    try {
      //var result = await MI.GET('http://www.miapi.cl/saludo);
      //CONSULTAR A LA API
      //SI OBTENEMOS RESULTADO CORRECTO, DEVOLVER DATOS
      //REALES DE LA CONEXIÓN.
      //return Example.fromJson(result);
      _logger.finest('DataRemoteExampleRepository successful');
      return Example(greeting: greeting);
    } catch (error, stackTrace) {
      _logger.warning('DataRemoteExampleRepository unsuccesful', error);
      rethrow;
    }
  }
}
