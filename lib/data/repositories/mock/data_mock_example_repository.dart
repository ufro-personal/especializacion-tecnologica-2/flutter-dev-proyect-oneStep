import 'package:logging/logging.dart';

import 'package:plantilla/domain/entities/example.dart';
import 'package:plantilla/domain/repositories/example_repository.dart';

class DataMockExampleRepository implements ExampleRepository {
  static final DataMockExampleRepository _instance =
      DataMockExampleRepository._internal();
  Logger _logger;

  DataMockExampleRepository._internal()
      : _logger = Logger('DataMockExampleRepository');

  factory DataMockExampleRepository() => _instance;

  @override
  Future<Example> getGreeting(String greeting) async {
    try {
      _logger.finest('DataMockExampleRepository successful');
      return Example(greeting: greeting);
    } catch (error, stackTrace) {
      _logger.warning('DataMockExampleRepository unsuccesful', error);
      rethrow;
    }
  }
}
