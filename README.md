# package: plantilla

*Proyecto OneStep.*

## Comencemos

- Es necesario previa a la ejecuccion, la creacion de un emulador a traves de Android Studio.

- Recomendamos por las dimensiones del prototipo de app, la instalacion del emulador de PIXEL 4 API 31.

- Y ademas recomendamos ejecutar:  <pre>flutter doctor</pre>
Para visualizar los requisitos de software y solucionarlos.

- En <code>VISUAL STUDIO CODE</code> con las teclas <pre>CTRL + SHIFT + P</pre>
Podremos desplegar una consulta de comandos donde seleccionaremos el SDK previamente configurado, seleccionar el dispositivo donde indicaremos que trabajaremos con un emulador.

- Para finalizar ejecutaremos <pre>flutter run</pre> para ejecutar el proyecto a traves de la terminal.

